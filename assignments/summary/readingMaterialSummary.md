# Introduction to Industrial IoT
* **The Industrial Internet of Things (IIoT)** refers to interconnected sensors, instruments, and other devices networked together with computers' industrial applications, including manufacturing and energy management.
* This connectivity allows for data collection, exchange, and analysis, potentially facilitating improvements in productivity and efficiency as well as other economic benefits.
<img src="https://rb.gy/8wxjyt">

## Industry 3.0
* Data is stored in databases and represented in excel sheets.
* Automation, Computers & Electronics.

<img src="https://rb.gy/kcoqjr">

## Industry3.0 Architecture
>
> **Sensors --> PLC's -->SCADA & ERP**
>
* Sensors installed at various points in theFactory send data to PLC's which collect allthe data and send it to SCADA and ERPsystems for storing the data.
* Usually this data is stored in Excels andCSV's and rearely get plotted as real-timegraphs or charts

<img src="https://rb.gy/f3srje">

## Industry 3.0 communication protocols
All these protocols areoptimized for sending datato a central server inside thefactory.

<img src="https://rb.gy/qw4lsj">

## Industry 4.0
* In a very basic sense, Industry 4.0 is Industry 3.0 devices connected to the Internet, which is what we, very elegantly call IoT.

* The main purpose of connected devices to the internet is the ability to send and recieve data.

* This therefore brings in various core functionality to these devices:


1. **Dashboards**: It transforms, displays & organizes a collection of data captured & transmitted by devices connected to the internet.

2. **Remote Web SCADA**: Enables users to remotely monitor & control remote devices.

3. Remote control configuration of devices.

4. Predictive maintenance.

5. Real-time event processing.

6. Analytics with predictive models.

7. Automated device provisioning (Auto discovery).

8. Real-time alerts & alarms.

<img src="https://rb.gy/vskjnw">

### Architecture
<img src="https://rb.gy/pumtny">

<img src="https://rb.gy/5fxm9c">

## How to convert Industry 3.0 to Industry 4.0
<img src="https://rb.gy/mkmttq">

### Basic structure of Industry 4.0

<img src="https://rb.gy/2y4g3y">

## What next?

Analyze data using various tools available online:


**IoT TSDB Tools**

These tools are used to store our data in the form of Time Series Databases. In a very basic sense, Time series data are simply measurements or events that are tracked, monitored, downsampled, and aggregated over time. This could be server metrics, application performance monitoring, network data, sensor data, events, clicks, trades in a market, and many other types of analytics data.
For example, Prometheus, InfluxDB, etc.


**IoT Dashboards**

Dashboards allow us to view our data in beautiful dashboards.
For example, Grafana, Thingsboard, etc.


**IoT Platforms**

Platforms allow us to analyse our data.
For example, AWS IoT, Google IoT, Azure IoT, Thingsboard, etc.


**Recieve Alerts**

Zaiper, Twilio, etc., allow us to get alerts based on our data using these platforms.